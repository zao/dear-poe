extern crate nalgebra_glm as glm;

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct Vertex {
    pub pos: glm::Vec3,
    pub clr: glm::Vec3,
}

impl Vertex {
    fn new(pos: glm::Vec3, clr: glm::Vec3) -> Self {
        Vertex { pos, clr }
    }
}

pub fn make_pyramid(extents: glm::Vec3) -> Vec<Vertex> {
    let d_x: glm::Vec3 = [extents.x, 0.0, 0.0].into();
    let d_y: glm::Vec3 = [0.0, extents.y, 0.0].into();
    let d_z: glm::Vec3 = [0.0, 0.0, -extents.z].into();

    let v_low: glm::Vec3 = [-extents.x / 2.0, -extents.y / 2.0, 0.0].into();

    let p = [
        v_low,
        v_low + d_x,
        v_low + d_y,
        v_low + d_x + d_y,
        v_low + 0.5 * d_x + 0.5 * d_y + d_z,
    ];

    let c = [[0.5, 0.5, 0.5].into(), [1.0, 1.0, 1.0].into()];

    [
        (0, 0),
        (4, 1),
        (1, 0),
        (1, 0),
        (4, 1),
        (3, 0),
        (3, 0),
        (4, 1),
        (2, 0),
        (2, 0),
        (4, 1),
        (0, 0),
        (0, 0),
        (1, 0),
        (2, 0),
        (2, 0),
        (1, 0),
        (3, 0),
    ]
    .iter()
    .map(|(p_idx, c_idx)| Vertex::new(p[*p_idx], c[*c_idx]))
    .collect()
}
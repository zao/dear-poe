use byteorder::LE;
use poe::formats::ggpk;
use std::path::Path;
use std::sync::Arc;

use byteorder::{ReadBytesExt, WriteBytesExt};
use ggpk::NodeInfo;

pub struct Caches {
    pub arms: Vec<u64>,
    pub smds: Vec<u64>,
}

impl Caches {
    pub fn new<P: AsRef<Path>>(pack: Arc<ggpk::Ggpk>, cache_dir: P) -> failure::Fallible<Self> {
        let root_digest = pack.root_digest().unwrap();

        let arm_cache_filename = cache_dir
            .as_ref()
            .join(format!("arm_cache-{}.bin", root_digest));
        let arms = if let Ok(contents) = std::fs::read(&arm_cache_filename) {
            let mut reader = &contents[..];
            let mut offsets = Vec::new();
            while let Ok(offset) = reader.read_u64::<LE>() {
                offsets.push(offset);
            }
            offsets
        } else {
            let (tx, rx) = crossbeam::channel::bounded(32);
            let pack = pack.clone();
            let t = std::thread::spawn(move || {
                poe::util::visit_ggpk_files(&pack, tx, "/").unwrap();
            });

            use std::io::BufWriter;

            let mut fh = BufWriter::new(std::fs::File::create(&arm_cache_filename).unwrap());
            let mut offsets = Vec::new();
            while let Ok((_dir, file)) = rx.recv() {
                if file.name().to_lowercase().ends_with(".arm") {
                    fh.write_u64::<LE>(file.offset).unwrap();
                    offsets.push(file.offset);
                }
            }

            t.join().unwrap();
            offsets
        };

        let smds = {
            let (tx, rx) = crossbeam::channel::bounded(128);
            let pack = pack.clone();
            let t = std::thread::spawn(move || {
                poe::util::visit_ggpk_files(&pack, tx, "/").unwrap();
            });

            let mut offsets = Vec::new();
            while let Ok((_dir, file)) = rx.recv() {
                if file.name().to_lowercase().ends_with(".smd") {
                    offsets.push(file.offset);
                }
            }

            t.join().unwrap();
            offsets
        };

        Ok(Caches {
            arms,
            smds,
        })
    }
}
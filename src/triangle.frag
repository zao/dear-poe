#version 330 core

in VS_OUTPUT {
    vec3 Normal;
    vec2 Texcoord;
} IN;

out vec4 Color;

uniform sampler2D ColorTex;
uniform sampler2D NormalTex;
uniform sampler2D SpecularTex;

void main()
{
    Color = vec4(IN.Texcoord, 0.0f, 1.0f);
    Color = texture(ColorTex, IN.Texcoord);
}
pub mod caches;
pub mod draw;
pub mod geometry;
mod mode;
pub mod render_gl;
mod util;

#[macro_use]
extern crate failure;

#[macro_use]
extern crate imgui;

extern crate lmdb_zero as lmdb;
extern crate nalgebra_glm as glm;

use poe::formats::{arm, dat, fmt, ggpk, mat, sm, smd};

use std::collections::HashMap;
use std::ffi::CString;
use std::rc::Rc;
use std::sync::Arc;

use structopt::StructOpt;

use ggpk::NodeInfo;

fn read_fmt_string(fmt: &fmt::Fmt, offset: usize) -> Option<String> {
    fmt.string_data
        .get(offset..)
        .and_then(|s| s.split(|x| *x == 0).next())
        .map(String::from_utf16_lossy)
}

struct Framebuffer {
    width: i32,
    height: i32,
    color_tex: gl::types::GLuint,
    depth_rbo: gl::types::GLuint,
    fbo: gl::types::GLuint,
}

impl Drop for Framebuffer {
    fn drop(&mut self) {
        unsafe { gl::DeleteFramebuffers(1, &self.fbo) };
        unsafe { gl::DeleteTextures(1, &self.color_tex) };
        unsafe { gl::DeleteRenderbuffers(1, &self.depth_rbo) };
    }
}

impl Framebuffer {
    fn new(size: (u32, u32)) -> Self {
        let width = size.0 as gl::types::GLsizei;
        let height = size.1 as gl::types::GLsizei;
        unsafe {
            let mut color_tex = 0;
            gl::CreateTextures(gl::TEXTURE_2D, 1, &mut color_tex);
            gl::TextureParameteri(color_tex, gl::TEXTURE_BASE_LEVEL, 0);
            gl::TextureParameteri(color_tex, gl::TEXTURE_MAX_LEVEL, 0);
            gl::TextureParameteri(color_tex, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
            gl::TextureParameteri(color_tex, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
            gl::TextureParameteri(color_tex, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_BORDER as i32);
            gl::TextureParameteri(color_tex, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_BORDER as i32);
            gl::TextureStorage2D(color_tex, 1, gl::RGBA8, width, height);

            let mut depth_rbo = 0;
            gl::CreateRenderbuffers(1, &mut depth_rbo);
            gl::NamedRenderbufferStorage(depth_rbo, gl::DEPTH_COMPONENT32F, width, height);

            let mut fbo = 0;
            gl::CreateFramebuffers(1, &mut fbo);
            gl::NamedFramebufferTexture(fbo, gl::COLOR_ATTACHMENT0, color_tex, 0);
            gl::NamedFramebufferRenderbuffer(
                fbo,
                gl::DEPTH_ATTACHMENT,
                gl::RENDERBUFFER,
                depth_rbo,
            );

            match gl::CheckNamedFramebufferStatus(fbo, gl::FRAMEBUFFER) {
                gl::FRAMEBUFFER_COMPLETE => {
                    eprintln!("FBO complete - {} x {}.", width, height);
                }
                status => {
                    eprintln!("{}", status);
                }
            }
            Framebuffer {
                width,
                height,
                fbo,
                color_tex,
                depth_rbo,
            }
        }
    }

    fn size(&self) -> (u32, u32) {
        (self.width as u32, self.height as u32)
    }

    fn bind(&self) {
        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, self.fbo);
        }
    }

    fn unbind(&self) {
        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
        }
    }
}

extern "system" fn debug_output_callback(
    source: gl::types::GLenum,
    typ: gl::types::GLenum,
    id: gl::types::GLenum,
    severity: gl::types::GLenum,
    length: gl::types::GLsizei,
    msg: *const gl::types::GLchar,
    _param: *mut gl::types::GLvoid,
) {
    let msg = unsafe { std::ffi::CStr::from_ptr(msg).to_str().unwrap() };
    if severity == gl::DEBUG_SEVERITY_HIGH {
        eprintln!(
            "OpenGL error - source: {}, type: {}, id: {}, severity: {}, length: {}, msg: {}",
            source, typ, id, severity, length, msg
        );
        panic!();
    }
}

struct VRState {
    left: Framebuffer,
    right: Framebuffer,
}

fn gather_file_set(
    pack: Arc<ggpk::Ggpk>,
    extension: &str,
    root: Option<&str>,
) -> Vec<(String, ggpk::File)> {
    crossbeam::thread::scope(|scope| {
        let (file_tx, file_rx) = crossbeam::channel::unbounded::<(ggpk::Directory, ggpk::File)>();
        let t = {
            let pack = pack.clone();
            scope.spawn(move |_| {
                file_rx
                    .iter()
                    .filter_map(|(_dir, file)| {
                        if file.name().to_lowercase().ends_with(extension) {
                            let full_path = poe::util::find_full_path(&pack, file.offset);
                            Some((full_path, file))
                        } else {
                            None
                        }
                    })
                    .collect::<Vec<_>>()
            })
        };
        poe::util::visit_ggpk_files(&pack, file_tx, root.unwrap_or("/")).unwrap();
        let mut v = t.join().unwrap();
        v.sort_by_cached_key(|t| t.0.clone());
        v
    })
    .unwrap()
}

#[derive(StructOpt)]
struct Opt {
    #[structopt(long = "vr")]
    vr: bool,
}

fn main() -> failure::Fallible<()> {
    let opt = Opt::from_args();
    use winapi::um::shellscalingapi::{SetProcessDpiAwareness, PROCESS_PER_MONITOR_DPI_AWARE};
    unsafe {
        SetProcessDpiAwareness(PROCESS_PER_MONITOR_DPI_AWARE);
    }

    let vr_ctx = if opt.vr {
        Some(unsafe { openvr::init(openvr::ApplicationType::Scene)? })
    } else {
        None
    };

    let vr_system = vr_ctx.as_ref().and_then(|ctx| ctx.system().ok());
    let vr_compo = vr_ctx.as_ref().and_then(|ctx| ctx.compositor().ok());

    let pack_path = r#"C:\Games\Path of Exile (Standalone)\Content.ggpk"#;
    let pack = ggpk::Ggpk::open(pack_path)?;

    let proj_dirs = directories::ProjectDirs::from("se.zao", "zao", "dear-arm").unwrap();
    let cache_dir = proj_dirs.cache_dir();
    std::fs::create_dir_all(cache_dir)?;

    let config_dir = proj_dirs.config_dir();
    std::fs::create_dir_all(config_dir)?;

    let sdl = sdl2::init().unwrap();
    let video_sys = sdl.video().unwrap();
    let _timer_sys = sdl.timer().unwrap();

    let _gl_attr = video_sys.gl_attr();
    _gl_attr.set_context_profile(sdl2::video::GLProfile::Core);
    _gl_attr.set_context_version(4, 5);
    _gl_attr.set_depth_size(0);

    let window = video_sys
        .window("dear-arm", 1536, 1024)
        .opengl()
        .resizable()
        .position_centered()
        .maximized()
        .allow_highdpi()
        .build()
        .unwrap();

    let _gl_ctx = window.gl_create_context().unwrap();
    gl::load_with(|s| video_sys.gl_get_proc_address(s) as _);

    unsafe {
        gl::DebugMessageCallback(Some(debug_output_callback), std::ptr::null());
        gl::Enable(gl::DEBUG_OUTPUT);
        gl::Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS);
    }

    let size = window.drawable_size();
    let mut screen_framebuffer = Framebuffer::new(size);
    let vr_state = vr_system.as_ref().map(|sys| {
        let size = sys.recommended_render_target_size();
        let left = Framebuffer::new(size);
        let right = Framebuffer::new(size);
        VRState { left, right }
    });

    let tri_prog = {
        let tri_vs = render_gl::Shader::from_vert_source(
            &CString::new(include_str!("triangle.vert")).unwrap(),
        )
        .unwrap();
        let tri_fs = render_gl::Shader::from_frag_source(
            &CString::new(include_str!("triangle.frag")).unwrap(),
        )
        .unwrap();
        Rc::new(render_gl::Program::from_shaders(&[tri_vs, tri_fs]).unwrap())
    };

    let color_prog = {
        let color_vs =
            render_gl::Shader::from_vert_source(&CString::new(include_str!("color.vert")).unwrap())
                .unwrap();
        let color_fs =
            render_gl::Shader::from_frag_source(&CString::new(include_str!("color.frag")).unwrap())
                .unwrap();
        Rc::new(render_gl::Program::from_shaders(&[color_vs, color_fs]).unwrap())
    };

    let arm_drawer = draw::ArmDrawer::new(tri_prog.clone());
    let smd_drawer = draw::SmdDrawer::new(tri_prog.clone());
    let line_drawer = draw::ColorLineDrawer::new(color_prog.clone(), 5000);

    let mut mode: Option<mode::ProgramMode> = mode::ProgramMode::hideout();

    let hideouts_dat =
        dat::parse_dat::<dat::spec::Hideouts>(pack.lookup_file("Data/Hideouts.dat")?.contents())?;
    let world_areas_dat = dat::parse_dat::<dat::spec::WorldAreas>(
        pack.lookup_file("Data/WorldAreas.dat")?.contents(),
    )?;
    let topologies_dat = dat::parse_dat::<dat::spec::Topologies>(
        pack.lookup_file("Data/Topologies.dat")?.contents(),
    )?;

    let mut file_sets = HashMap::new();
    file_sets.insert("arm", gather_file_set(pack.clone(), ".arm", None));
    file_sets.insert("fmt", gather_file_set(pack.clone(), ".fmt", Some("Art")));
    // file_sets.insert("sm", gather_file_set(pack.clone(), ".sm", None).into_iter().filter(|x| x.0.contains("bees")).collect());
    file_sets.insert("sm", gather_file_set(pack.clone(), ".sm", None));
    file_sets.insert("smd", gather_file_set(pack.clone(), ".smd", None));

    let arm_files = file_sets["arm"].clone();

    let arm_paths = arm_files
        .iter()
        .map(|file| file.0.clone().into())
        .collect::<Vec<imgui::ImString>>();

    let smd_files = file_sets["smd"].clone();

    let smd_paths = smd_files
        .iter()
        .map(|file| file.0.clone().into())
        .collect::<Vec<imgui::ImString>>();

    let mut imgui = imgui::Context::create();
    let ini_path = config_dir.join("imgui.ini");
    imgui.set_ini_filename(Some(ini_path));
    let fontin = {
        let font_bytes = pack.lookup_file("Art/2DArt/Fonts/Fontin-Regular.ttf")?;
        imgui.fonts().add_font(&[imgui::FontSource::TtfData {
            data: font_bytes.contents(),
            size_pixels: 16.0,
            config: None,
        }])
    };

    let mut imgui_sdl2 = imgui_sdl2::ImguiSdl2::new(&mut imgui, &window);

    let renderer =
        imgui_opengl_renderer::Renderer::new(&mut imgui, |s| video_sys.gl_get_proc_address(s) as _);

    let mut event_pump = sdl.event_pump().unwrap();
    'main: loop {
        use sdl2::event::Event;
        use sdl2::keyboard::Keycode;
        for event in event_pump.poll_iter() {
            imgui_sdl2.handle_event(&mut imgui, &event);
            if imgui_sdl2.ignore_event(&event) {
                continue;
            }
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'main,
                _ => {}
            }
        }

        vr_compo
            .as_ref()
            .map(|compo| compo.wait_get_poses().unwrap());

        if let Some(sys) = vr_system.as_ref() {
            while let Some((ev, _pose)) =
                sys.poll_next_event_with_pose(openvr::TrackingUniverseOrigin::Standing)
            {
                eprintln!("{:?} {} {:?}", ev.tracked_device_index, ev.age, ev.event);
            }
        }

        let win_size = window.drawable_size();

        if win_size != screen_framebuffer.size() {
            screen_framebuffer = Framebuffer::new(win_size);
        }
        screen_framebuffer.bind();
        unsafe {
            gl::Viewport(0, 0, screen_framebuffer.width, screen_framebuffer.height);
            gl::ClearColor(0.3, 0.2, 0.1, 1.0);
            gl::ClearDepth(1.0);
        }

        imgui_sdl2.prepare_frame(imgui.io_mut(), &window, &event_pump.mouse_state());
        let ui = imgui.frame();
        let _font = ui.push_font(fontin);
        let mut should_quit = false;
        ui.main_menu_bar(|| {
            ui.menu(im_str!("File"), true, || {
                if imgui::MenuItem::new(im_str!("Exit")).build(&ui) {
                    should_quit = true;
                }
            });
            ui.menu(im_str!("Mode"), true, || {
                let is_hideout = match mode {
                    Some(mode::ProgramMode::Hideout(_)) => true,
                    _ => false,
                };
                if imgui::MenuItem::new(im_str!("Hideout"))
                    .selected(is_hideout)
                    .build(&ui)
                {
                    mode = mode::ProgramMode::hideout();
                }

                let is_item = match mode {
                    Some(mode::ProgramMode::Item(_)) => true,
                    _ => false,
                };
                if imgui::MenuItem::new(im_str!("Item"))
                    .selected(is_item)
                    .build(&ui)
                {
                    mode = mode::ProgramMode::item();
                }

                let is_sm = match mode {
                    Some(mode::ProgramMode::Sm(_)) => true,
                    _ => false,
                };
                if imgui::MenuItem::new(im_str!("SM"))
                    .selected(is_sm)
                    .build(&ui)
                {
                    mode = mode::ProgramMode::sm();
                }

                let is_arm = match mode {
                    Some(mode::ProgramMode::Arm(_)) => true,
                    _ => false,
                };
                if imgui::MenuItem::new(im_str!("ARM"))
                    .selected(is_arm)
                    .build(&ui)
                {
                    mode = mode::ProgramMode::arm();
                }

                let is_smd = match mode {
                    Some(mode::ProgramMode::Smd(_)) => true,
                    _ => false,
                };
                if imgui::MenuItem::new(im_str!("SMD"))
                    .selected(is_smd)
                    .build(&ui)
                {
                    mode = mode::ProgramMode::smd();
                }

                let is_fmt = match mode {
                    Some(mode::ProgramMode::Fmt(_)) => true,
                    _ => false,
                };
                if imgui::MenuItem::new(im_str!("FMT"))
                    .selected(is_fmt)
                    .build(&ui)
                {
                    mode = mode::ProgramMode::fmt();
                }
            });

            // Mode specific menu
            if let Some(mode::ProgramMode::Hideout(ref mut _mode)) = mode {
                ui.menu(im_str!("Hideout"), true, || {});
            }
        });
        if should_quit {
            break 'main;
        }

        match mode.as_mut() {
            Some(mode::ProgramMode::Item(ref mut _mode)) => {
                imgui::Window::new(im_str!("Items"))
                    .size([600.0, 600.0], imgui::Condition::FirstUseEver)
                    .build(&ui, || {
                        ui.text("hi!");
                    });
            }
            Some(mode::ProgramMode::Sm(ref mut mode)) => {
                imgui::Window::new(im_str!("SM files"))
                    .size([600.0, 600.0], imgui::Condition::FirstUseEver)
                    .build(&ui, || {
                        let sms = &file_sets["sm"];
                        let (paths, files): (Vec<imgui::ImString>, Vec<ggpk::File>) = sms
                            .iter()
                            .map(|(path, f)| (imgui::ImString::new(path.clone()), f.clone()))
                            .unzip();
                        let path_strs: Vec<&imgui::ImStr> =
                            paths.iter().map(|s| s.as_ref()).collect();

                        imgui::ComboBox::new(im_str!("SM file")).build_simple_string(
                            &ui,
                            &mut mode.desired_index,
                            &path_strs,
                        );

                        let new_path = &path_strs[mode.desired_index];
                        let already_loaded = match &mode.current_sm {
                            Some(lsm) => lsm.path == new_path.to_str(),
                            _ => false,
                        };
                        if !already_loaded {
                            let res = load_sm(pack.clone(), &files[mode.desired_index]);
                            mode.current_sm = res.ok();
                        }

                        if let Some(sm) = &mode.current_sm {
                            for mat in &sm.material_sources {
                                ui.text(format!("{:#?}", mat));
                            }
                        }
                    });
            }
            Some(mode::ProgramMode::Fmt(ref mut mode)) => {
                imgui::Window::new(im_str!("FMT files"))
                    .size([600.0, 600.0], imgui::Condition::FirstUseEver)
                    .build(&ui, || {
                        let table = &file_sets["fmt"];
                        let item_names: Vec<imgui::ImString> = table
                            .iter()
                            .map(|(name, _file)| imgui::ImString::new(name.clone()))
                            .collect();
                        let item_strs: Vec<&imgui::ImStr> =
                            item_names.iter().map(|s| s.as_ref()).collect();

                        imgui::ComboBox::new(im_str!("FMT file")).build_simple_string(
                            &ui,
                            &mut mode.desired_index,
                            &item_strs,
                        );

                        if mode.current_index != Some(mode.desired_index) {
                            mode.current_index = Some(mode.desired_index);
                            let file = &table[mode.desired_index].1;
                            let data = poe::util::try_decompress(file.contents());
                            mode.fmt = fmt::parse_fmt(&data).ok();

                            if let Some(fmt) = &mode.fmt {
                                let range_count = fmt.shapes.len();
                                mode.draw_geom = Some(draw::IndexedGeom::from_fmt(&fmt));
                                let mut required_materials = vec![];
                                mode.draw_info = Some(draw::SmdInfo {
                                    shape_ranges: {
                                        let offsets = fmt.shapes.iter().map(|s| s.triangle_offset);
                                        let index_starts: Vec<i32> = offsets
                                            .map(|off| off as i32 * 3)
                                            .chain(std::iter::once(
                                                mode.draw_geom.as_ref().unwrap().element_count
                                                    as i32,
                                            ))
                                            .collect();
                                        index_starts
                                            .windows(2)
                                            .map(|offs| {
                                                let from = offs[0];
                                                let to = offs[1];
                                                let len = to - from;
                                                (from, len)
                                            })
                                            .collect()
                                    },
                                    shape_enables: std::iter::repeat(true)
                                        .take(range_count)
                                        .collect(),
                                    shape_materials: fmt
                                        .shapes
                                        .iter()
                                        .map(|shape| {
                                            let mat_name = read_fmt_string(
                                                &fmt,
                                                shape.material_offset as usize,
                                            )
                                            .unwrap();
                                            if let Some(pos) = required_materials
                                                .iter()
                                                .position(|x| x == &mat_name)
                                            {
                                                pos
                                            } else {
                                                required_materials.push(mat_name);
                                                required_materials.len()
                                            }
                                        })
                                        .collect(),
                                });
                                eprintln!("{:?}", required_materials);
                                for name in &required_materials {
                                    if let ggpk::ResolvedEntry::File(file) =
                                        pack.lookup(name).unwrap()
                                    {
                                        let m = mat::parse_mat(file.contents());
                                        eprintln!("{:?}", m);
                                    }
                                }
                            }
                        }

                        if let Some(fmt) = &mode.fmt {
                            ui.text(im_str!("Version: {}", fmt.version));
                            ui.text(im_str!("d: {:?}", fmt.d));
                            let bbox = &fmt.bbox;
                            ui.text(im_str!(
                                "bbox: X ({}, {}), Y ({}, {}), Z ({}, {})",
                                bbox[0],
                                bbox[1],
                                bbox[2],
                                bbox[3],
                                bbox[4],
                                bbox[5]
                            ));
                            ui.text(im_str!("Shapes:"));
                            for (i, shape) in fmt.shapes.iter().enumerate() {
                                let name = read_fmt_string(&fmt, shape.name_offset as usize);
                                let material_name =
                                    read_fmt_string(&fmt, shape.material_offset as usize);
                                ui.text(im_str!(
                                    "  {}: name: {:?}, material: {:?}, first triangle: {}",
                                    i,
                                    name,
                                    material_name,
                                    shape.triangle_offset
                                ));
                            }
                            if !fmt.d0s.is_empty() {
                                ui.text(im_str!("D0s:"));
                                for (i, d0) in fmt.d0s.iter().enumerate() {
                                    ui.text(im_str!("  {}: {:?}", i, d0.unk0));
                                }
                            }
                            ui.text(im_str!(
                                "Indices: {}",
                                match &fmt.indices {
                                    fmt::Indices::Long(is) => is.len(),
                                    fmt::Indices::Short(is) => is.len(),
                                }
                            ));
                            ui.text(im_str!("Vertices: {}", fmt.vertices.len()));
                            if !fmt.d1s.is_empty() {
                                ui.text(im_str!("D1s:"));
                                for (i, d1) in fmt.d1s.iter().enumerate() {
                                    ui.text(im_str!("  {}: {:?}, {}", i, d1.unk0, d1.f8));
                                }
                            }
                            if !fmt.d3s.is_empty() {
                                ui.text(im_str!("D3s:"));
                                for (i, d3) in fmt.d3s.iter().enumerate() {
                                    ui.text(im_str!(
                                        "  {}: {:?}, {:?}, {:?}",
                                        i,
                                        d3.x,
                                        d3.f,
                                        d3.unk
                                    ));
                                }
                            }
                            if !fmt.string_table.is_empty() {
                                ui.text(im_str!("Strings:"));
                                for (i, s) in fmt.string_table.iter().enumerate() {
                                    ui.text(im_str!("  {}: \"{}\"", i, s));
                                }
                            }
                        }
                    });
            }
            Some(mode::ProgramMode::Hideout(ref mut mode)) => {
                imgui::Window::new(im_str!("Hideouts"))
                    .size([600.0, 600.0], imgui::Condition::FirstUseEver)
                    .build(&ui, || {
                        let item_names: Vec<imgui::ImString> = hideouts_dat
                            .rows
                            .iter()
                            .map(|row| imgui::ImString::new(row.Name.clone()))
                            .collect();
                        let item_strs: Vec<&imgui::ImStr> =
                            item_names.iter().map(|s| s.as_ref()).collect();

                        imgui::ComboBox::new(im_str!("Hideout")).build_simple_string(
                            &ui,
                            &mut mode.desired_index,
                            &item_strs,
                        );

                        if mode.current_index != Some(mode.desired_index) {
                            mode.current_index = Some(mode.desired_index);
                            mode.current_data = Some(|| -> failure::Fallible<_> {
                                Ok({
                                    let hideout_dat = hideouts_dat.rows[mode.desired_index].clone();
                                    let small_world_area_dat = world_areas_dat.rows
                                        [hideout_dat.SmallWorldAreasKey as usize]
                                        .clone();
                                    let first_topologies_key =
                                        small_world_area_dat.TopologiesKeys.first().unwrap();
                                    let topology_dat =
                                        topologies_dat.rows[*first_topologies_key as usize].clone();
                                    let hideout = poe::formats::hideout::parse_hideout(
                                        pack.lookup_file(&hideout_dat.HideoutFile)?.contents(),
                                    )?;
                                    let graph = {
                                        let filename = &topology_dat.DGRFile;
                                        let contents = poe::util::vec_to_utf16(
                                            pack.lookup_file(filename)?.contents(),
                                        )
                                        .ok_or_else(|| format_err!("DgrFile not valid UTF-16"))?;
                                        if filename.ends_with(".dgr") {
                                            mode::Graph::Dgr(poe::formats::dgr::parse_dgr(
                                                &contents,
                                            )?)
                                        } else if filename.ends_with(".tgr") {
                                            mode::Graph::Tgr(poe::formats::tgr::parse_tgr(
                                                &contents,
                                            )?)
                                        } else {
                                            bail!("DgrFile not a DGR or TGR file");
                                        }
                                    };

                                    let master_file = match &graph {
                                        mode::Graph::Dgr(ref dgr) => dgr.master_file.clone(),
                                        mode::Graph::Tgr(ref tgr) => tgr.master_file.clone(),
                                    };
                                    let tsi = {
                                        let contents = poe::util::vec_to_utf16(
                                            pack.lookup_file(&master_file)?.contents(),
                                        )
                                        .ok_or_else(|| format_err!("TSI not valid UTF-16"))?;
                                        poe::formats::tsi::parse_tsi(&contents)?
                                    };
                                    let master_root = master_file.rsplitn(2, '/').nth(1).unwrap();

                                    let rs = {
                                        let path = poe::util::strip_quotes(&tsi.fields["RoomSet"])
                                            .ok_or_else(|| {
                                                format_err!("RS path not quoted in TSI")
                                            })?;
                                        eprintln!("Path: {}", &path);
                                        let file = pack.lookup_file(&path).or_else(|_| {
                                            let path = format!("{}/{}", &master_root, &path);
                                            eprintln!("Path: {}", &path);
                                            pack.lookup_file(path)
                                        })?;
                                        eprintln!("File: {:?}", &file);
                                        let contents = poe::util::vec_to_utf16(file.contents())
                                            .ok_or_else(|| format_err!("RS not valid UTF-16"))?;
                                        poe::formats::rs::parse_rs(&contents)?
                                    };
                                    mode::HideoutData {
                                        hideout_dat,
                                        small_world_area_dat,
                                        topology_dat,
                                        hideout,
                                        graph,
                                        tsi,
                                        rs,
                                    }
                                })
                            }());
                        }

                        match &mode.current_data {
                            Some(Ok(ref data)) => {
                                if ui.collapsing_header(im_str!("Hideouts.dat entry")).build() {
                                    ui.text(format!("{:#?}", data.hideout_dat));
                                }
                                if ui.collapsing_header(im_str!("Roomset")).build() {
                                    ui.text(format!("{:#?}", data.rs));
                                }
                                if ui.collapsing_header(im_str!("TSI")).build() {
                                    ui.text(format!("{:#?}", data.tsi));
                                }
                                if ui.collapsing_header(im_str!("Graph")).build() {
                                    match &data.graph {
                                        mode::Graph::Dgr(ref dgr) => ui.text(format!("{:#?}", dgr)),
                                        mode::Graph::Tgr(ref tgr) => ui.text(format!("{:#?}", tgr)),
                                    }
                                }
                                if ui
                                    .collapsing_header(im_str!("Topologies.dat entry"))
                                    .build()
                                {
                                    ui.text(format!("{:#?}", data.topology_dat));
                                }
                                if ui
                                    .collapsing_header(im_str!("Small WorldAreas.dat entry"))
                                    .build()
                                {
                                    ui.text(format!("{:#?}", data.small_world_area_dat));
                                }
                                if ui.collapsing_header(im_str!("Hideout blueprint")).build() {
                                    ui.text(format!("{:#?}", data.hideout));
                                }
                            }
                            Some(Err(e)) => {
                                ui.text(format!("Error: {:?}", e));
                            }
                            None => {
                                ui.text("Nothing loaded yet.".to_string());
                            }
                        }
                    });
            }
            Some(mode::ProgramMode::Arm(ref mut mode)) => {
                imgui::Window::new(im_str!("ARM files"))
                    .size([600.0, 600.0], imgui::Condition::FirstUseEver)
                    .build(&ui, || {
                        ui.input_text(im_str!("Filter"), &mut mode.search_filter.needle)
                            .build();
                        ui.checkbox(
                            im_str!("Case sensitive search"),
                            &mut mode.search_filter.case_sensitive,
                        );
                        let (id_map, strings): (Vec<usize>, Vec<&imgui::ImStr>) = arm_paths
                            .iter()
                            .map(|x| x.as_ref())
                            .enumerate()
                            .filter(mode.search_filter.predicate())
                            .unzip();
                        if !strings.is_empty() {
                            let mut selection = if let Some(pos) =
                                id_map.iter().position(|x| *x == mode.desired_arm_index)
                            {
                                pos as i32
                            } else {
                                0i32
                            };
                            ui.list_box(
                                &im_str!("ARMs ({}/{})", strings.len(), arm_paths.len()),
                                &mut selection,
                                &strings[..],
                                strings.len() as i32,
                            );
                            mode.desired_arm_index = id_map[selection as usize];
                        }
                    });

                if mode.current_arm_index != Some(mode.desired_arm_index) {
                    if let Some(text) =
                        poe::util::vec_to_utf16(&arm_files[mode.desired_arm_index].1.contents())
                    {
                        mode.current_arm_index = Some(mode.desired_arm_index);
                        let arm = arm::parse_arm(&text);
                        mode.change(arm.ok());
                    }
                }

                imgui::Window::new(im_str!("ARM info"))
                    .size([600.0, 600.0], imgui::Condition::FirstUseEver)
                    .build(&ui, || {
                        if let Some(res) = &mode.current_arm {
                            match res {
                                Ok(arm) => {
                                    ui.text(&arm_paths[mode.current_arm_index.unwrap() as usize]);
                                    ui.text(format!("Version: {}", arm.version));
                                    ui.text(format!("GT/ET: {}", arm.gtets.len()));
                                    for (i, gtet) in arm.gtets.iter().enumerate() {
                                        ui.text(format!(" {:2}: {:?}", i, gtet));
                                    }
                                    ui.text(format!("Dims: {:?}", arm.dims));
                                    ui.text(format!("a_upair: {:?}", arm.a_upair));
                                    ui.text(format!("name: {:?}", arm.name));
                                    ui.text(format!("b_upair: {:?}", arm.b_upair));
                                    ui.columns(1, im_str!(""), true);
                                    ui.text(format!("k_1_by_1: {:?}", arm.k_1_by_1));
                                    ui.columns(1, im_str!(""), false);
                                    ui.text(format!("c_uious: {:?}", arm.c_uious));
                                    for (i, ds) in arm.dentries.iter().enumerate() {
                                        ui.text(format!("Dentry #{}: {}", i + 1, ds.len()));
                                        for d in ds {
                                            ui.text(format!("  {:?}", d));
                                        }
                                    }
                                    ui.text(format!(
                                        "k_x_by_y: {}x{}",
                                        arm.k_x_by_y[0].len(),
                                        arm.k_x_by_y.len()
                                    ));
                                    for k_row in &arm.k_x_by_y {
                                        for (i, k_col) in k_row.iter().enumerate() {
                                            if i > 0 {
                                                ui.same_line(0.0);
                                            }
                                            let text = util::render_k_entry_text(k_col);
                                            ui.button(&im_str!("{}", text), [100.0, 100.0]);
                                        }
                                    }
                                    ui.text(format!("Tail lines: {}", arm.whole_tail.len()));
                                    for line in &arm.whole_tail {
                                        ui.text(line);
                                    }
                                    // ui.text(format!("doodads: {}", arm.doodads.len()));
                                    // for doodad in &arm.doodads {
                                    //     ui.text(format!("  {:?}", doodad));
                                    // }
                                    // ui.text(format!("decals: {}", arm.decals.len()));
                                    // for decal in &arm.decals {
                                    //     ui.text(format!("  {:?}", decal));
                                    // }
                                    // ui.text(format!("e1_usize: {:?}", arm.e1_usize));
                                    // ui.text(format!("e2_usize: {:?}", arm.e2_usize));
                                    // ui.text(format!("tail_line: {:?}", arm.tail_line));
                                }
                                Err(e) => {
                                    ui.text(format!("{:?}", e));
                                }
                            }
                        }
                    });
            }
            Some(mode::ProgramMode::Smd(ref mut mode)) => {
                imgui::Window::new(im_str!("SMD control"))
                    .size([200.0, 120.0], imgui::Condition::FirstUseEver)
                    .build(&ui, || {
                        if ui.button(im_str!("Previous"), [100.0, 20.0]) {
                            mode.desired_index += smd_files.len() - 1;
                            mode.desired_index %= smd_files.len();
                        }
                        ui.same_line(0.0);
                        if ui.button(im_str!("Next"), [100.0, 20.0]) {
                            mode.desired_index += 1;
                            mode.desired_index %= smd_files.len();
                        }
                        ui.same_line(0.0);
                        if ui.button(im_str!("Save"), [100.0, 20.0]) {
                            let f = &smd_files[mode.desired_index];
                            let mut p: std::path::PathBuf = r#"C:\Temp\SMD"#.into();
                            p.push(&f.0);
                            std::fs::create_dir_all(p.parent().unwrap()).unwrap();
                            std::fs::write(p, poe::util::decompress(f.1.contents()).unwrap())
                                .unwrap();
                        }
                        ui.text(im_str!(
                            "{}: {}",
                            mode.desired_index,
                            smd_paths[mode.desired_index].to_str()
                        ));
                    });

                if Some(mode.desired_index) != mode.current_index {
                    let idx = mode.desired_index;
                    eprintln!("{}: {}", idx, smd_files[idx].0);
                    mode.current_smd = Some(smd::parse_smd(smd_files[idx].1.contents()));
                    mode.current_index = Some(mode.desired_index);
                    if let Some(Ok(smd)) = &mode.current_smd {
                        let range_count = smd.shape_triangle_offsets.len();
                        mode.geom = Some(draw::IndexedGeom::new(&smd));
                        mode.info = Some(draw::SmdInfo {
                            shape_ranges: {
                                let offsets = &smd.shape_triangle_offsets;
                                let index_starts: Vec<i32> = offsets
                                    .iter()
                                    .map(|off| *off as i32 * 3)
                                    .chain(std::iter::once(
                                        mode.geom.as_ref().unwrap().element_count as i32,
                                    ))
                                    .collect();
                                index_starts
                                    .windows(2)
                                    .map(|offs| {
                                        let from = offs[0];
                                        let to = offs[1];
                                        let len = to - from;
                                        (from, len)
                                    })
                                    .collect()
                            },
                            shape_enables: std::iter::repeat(true).take(range_count).collect(),
                            shape_materials: vec![],
                        });
                    }
                }
            }
            None => {}
        }

        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
            gl::Enable(gl::DEPTH_TEST);
            gl::Enable(gl::CULL_FACE);
            gl::FrontFace(gl::CW);
            gl::CullFace(gl::BACK);
        }

        let aspect = win_size.0 as f32 / win_size.1 as f32;
        let projection: glm::Mat4 =
            glm::perspective_fov_lh(glm::pi::<f32>() / 2.0, aspect, 1.0, 10.0, 1000.0);

        match &mode {
            Some(mode::ProgramMode::Item(ref _mode)) => {}
            Some(mode::ProgramMode::Sm(ref mode)) => {
                if let Some(sm) = &mode.current_sm {
                    let bbox = &sm.smd_source.bbox;
                    let ext = glm::vec3(bbox[1] - bbox[0], bbox[3] - bbox[2], bbox[5] - bbox[4]);
                    let center = glm::vec3(bbox[0], bbox[2], bbox[4]) + ext / 2.0;
                    let zoom_dist = glm::max(&ext, 0.01).magnitude() / 2.0;
                    let eye = center + glm::vec3(-1.0, -1.0, -2.0).normalize() * zoom_dist;
                    let up = glm::vec3(1.0, 1.0, 0.0).normalize();
                    let view = glm::look_at_lh(&eye, &center, &up);
                    let pos = glm::vec3(0.0, 0.0, 0.0);
                    let rot = glm::Quat::identity();
                    smd_drawer.draw(
                        draw::ViewData {
                            projection: &projection,
                            view: &view,
                        },
                        &sm.smd_info,
                        &sm.smd_geom,
                        &sm.smd_materials,
                        draw::SpatialData {
                            position: &pos,
                            orientation: &rot,
                        },
                    )?;
                }
            }
            Some(mode::ProgramMode::Fmt(ref mode)) => {
                let bbox = mode.fmt.as_ref().unwrap().bbox;
                let ext = glm::vec3(bbox[1] - bbox[0], bbox[3] - bbox[2], bbox[5] - bbox[4]);
                let center = glm::vec3(bbox[0], bbox[2], bbox[4]) + ext / 2.0;
                let zoom_dist = glm::max(&ext, 0.01).magnitude();
                let eye = center + glm::vec3(-1.0, -1.0, -2.0).normalize() * zoom_dist;
                let up = glm::vec3(1.0, 1.0, 0.0).normalize();
                let view = glm::look_at_lh(&eye, &center, &up);
                // if let Some(geom) = &mode.draw_geom {
                //     let pos = glm::vec3(0.0, 0.0, 0.0);
                //     let rot = glm::Quat::identity();
                //     smd_drawer.draw(
                //         &projection,
                //         &view,
                //         mode.draw_info.as_ref().unwrap(),
                //         geom,
                //         &pos,
                //         &rot,
                //     )?;
                // }
            }
            Some(mode::ProgramMode::Hideout(ref mode)) => {
                if let Some(Ok(data)) = &mode.current_data {
                    const WAYPOINT_HASH: u32 = 1_224_707_366;
                    let mid = data
                        .hideout
                        .items
                        .iter()
                        .find(|item| item.hash == WAYPOINT_HASH)
                        .map(|item| glm::vec2(item.x as f32, item.y as f32))
                        .unwrap_or_else(glm::zero);
                    let center = glm::vec3(mid.x, mid.y, 0.0);
                    let eye = center + glm::vec3(-5.0, -5.0, -10.0) * 20.0;
                    let up = glm::vec3(1.0, 1.0, 0.0).normalize();
                    let view = glm::look_at_lh(&eye, &center, &up);
                    let mut line_draw_data: Vec<draw::ColorVertex> = vec![];
                    let room_unit_size = glm::make_vec2(
                        &data
                            .tsi
                            .fields
                            .get("RoomUnitSize")
                            .and_then(|rus| -> Option<[f32; 2]> {
                                let mut iter = rus.splitn(2, ' ').map(|x| x.parse::<f32>().ok());
                                Some([iter.next()??, iter.next()??])
                            })
                            .unwrap_or([1.0, 1.0]),
                    );
                    let corners: [glm::Vec3; 4] = [
                        [0.0, 0.0, 0.0].into(),                           //
                        [room_unit_size.x, 0.0, 0.0].into(),              //
                        [0.0, room_unit_size.y, 0.0].into(),              //
                        [room_unit_size.x, room_unit_size.y, 0.0].into(), //
                    ];
                    line_draw_data.extend([0, 1, 1, 3, 3, 2, 2, 0].iter().map(|idx| {
                        draw::ColorVertex {
                            pos: corners[*idx],
                            clr: glm::vec3(0.5, 0.5, 0.5),
                        }
                    }));

                    let resolution = match &data.graph {
                        mode::Graph::Dgr(ref dgr) => dgr.resolution,
                        mode::Graph::Tgr(ref tgr) => tgr.resolution,
                    };

                    line_draw_data.extend(
                        draw::GridBuilder::new(
                            glm::vec2(0.0, 0.0),
                            glm::vec2(resolution.0 as f32, resolution.1 as f32),
                            0.0,
                        )
                        .major_spacing(room_unit_size)
                        .minor_ratio(4)
                        .major_color(glm::vec3(0.5, 0.5, 0.5))
                        .minor_color(glm::vec3(0.3, 0.3, 0.3))
                        .build()
                        .into_iter(),
                    );
                    let nodes: Vec<_> = match &data.graph {
                        mode::Graph::Dgr(ref dgr) => dgr.nodes.iter().map(|n| (n.x, n.y)).collect(),
                        mode::Graph::Tgr(ref tgr) => tgr.nodes.iter().map(|n| (n.x, n.y)).collect(),
                    };

                    for node in &nodes {
                        line_draw_data.extend(
                            draw::DiamondBuilder::new(glm::vec3(node.0 as f32, node.1 as f32, 0.0))
                                .color(glm::vec3(0.6, 0.6, 0.0))
                                .size(5.0)
                                .build()
                                .into_iter(),
                        );
                    }

                    line_draw_data.extend(data.hideout.items.iter().flat_map(|item| {
                        draw::DiamondBuilder::new(glm::vec3(item.x as f32, item.y as f32, 0.0))
                            .color(glm::vec3(0.1, 0.5, 0.1))
                            .size(5.0)
                            .build()
                            .into_iter()
                    }));

                    line_drawer.draw(&projection, &view, &line_draw_data);
                }
            }
            Some(mode::ProgramMode::Arm(ref mode)) => {
                let mid = mode.current_mid;
                let center = glm::vec3(mid.x, mid.y, 0.0);
                let eye = center + glm::vec3(-5.0, -5.0, -10.0) * 20.0;
                let up = glm::vec3(1.0, 1.0, 0.0).normalize();
                let view = glm::look_at_lh(&eye, &center, &up);
                if let Some(geom) = &mode.geom {
                    arm_drawer.draw(&projection, &view, geom);
                }
            }
            Some(mode::ProgramMode::Smd(ref mode)) => {
                if let Some(geom) = &mode.geom {
                    let bbox = mode.current_smd.as_ref().unwrap().as_ref().unwrap().bbox;
                    let ext = glm::vec3(bbox[1] - bbox[0], bbox[3] - bbox[2], bbox[5] - bbox[4]);
                    let center = glm::vec3(bbox[0], bbox[2], bbox[4]) + ext / 2.0;
                    let zoom_dist = glm::max(&ext, 0.01).magnitude();
                    let eye = center + glm::vec3(-1.0, -1.0, -2.0).normalize() * zoom_dist;
                    let up = glm::vec3(1.0, 1.0, 0.0).normalize();
                    let view = glm::look_at_lh(&eye, &center, &up);
                    let pos = glm::vec3(0.0, 0.0, 0.0);
                    let rot = glm::Quat::identity();
                    // smd_drawer.draw(
                    //     &projection,
                    //     &view,
                    //     mode.info.as_ref().unwrap(),
                    //     geom,
                    //     &pos,
                    //     &rot,
                    // )?;
                }
                imgui::Window::new(im_str!("SMD info"))
                    .size([200.0, 120.0], imgui::Condition::FirstUseEver)
                    .build(&ui, || {
                        use std::fmt::Write;
                        let mut output = String::new();
                        for (i, (off, len)) in
                            mode.info.as_ref().unwrap().shape_ranges.iter().enumerate()
                        {
                            writeln!(&mut output, "Shape {}: {} + {}", i, off, len).unwrap();
                        }
                        ui.text(im_str!("{}", output));
                        output = String::new();
                        writeln!(
                            &mut output,
                            "{:#?}",
                            &mode.current_smd.as_ref().unwrap().as_ref().unwrap().tail
                        )
                        .unwrap();
                        ui.text(im_str!("{}", output));
                    });
            }
            None => {}
        }

        drop(_font);
        imgui_sdl2.prepare_render(&ui, &window);
        renderer.render(ui);
        screen_framebuffer.unbind();
        unsafe {
            gl::ClearColor(0.0, 0.5, 0.0, 0.0);
            gl::Clear(gl::COLOR_BUFFER_BIT);
            gl::BlitNamedFramebuffer(
                screen_framebuffer.fbo,
                0, //
                0,
                0,
                screen_framebuffer.width,
                screen_framebuffer.height, //
                0,
                0,
                win_size.0 as i32,
                win_size.1 as i32, //
                gl::COLOR_BUFFER_BIT,
                gl::NEAREST,
            );
        }

        if let Some(compo) = vr_compo.as_ref() {
            let fbs = vr_state.as_ref().unwrap();
            for (clr, fbo) in &[
                (glm::vec3(1.0, 0.0, 0.0), &fbs.left),
                (glm::vec3(0.0, 1.0, 0.0), &fbs.right),
            ] {
                fbo.bind();
                unsafe {
                    gl::ClearColor(clr.x, clr.y, clr.z, 1.0);
                    gl::Clear(gl::COLOR_BUFFER_BIT);
                }
            }

            use openvr::compositor::*;
            for (eye, tex) in &[
                (openvr::Eye::Left, fbs.left.color_tex),
                (openvr::Eye::Right, fbs.right.color_tex),
            ] {
                unsafe {
                    compo
                        .submit(
                            *eye,
                            &Texture {
                                handle: texture::Handle::OpenGLTexture(*tex as usize),
                                color_space: texture::ColorSpace::Auto,
                            },
                            None,
                            None,
                        )
                        .unwrap();
                }
            }
        }
        window.gl_swap_window();
    }

    Ok(())
}

fn load_sm(pack: Arc<ggpk::Ggpk>, file: &ggpk::File) -> failure::Fallible<mode::LoadedSm> {
    let path = poe::util::find_full_path(&pack, file.offset);
    let sm_source = sm::parse_sm(file.contents())?;
    // eprintln!("{:#?}", &sm_source);
    let smd_file = match pack.lookup(&sm_source.skinned_mesh_data)? {
        ggpk::ResolvedEntry::File(file) => file,
        _ => bail!("SMD file {} is a directory", &sm_source.skinned_mesh_data),
    };
    let smd_source = smd::parse_smd(smd_file.contents())?;
    let (material_sources, smd_materials, material_indices) = {
        let mut sources = vec![];
        let mut materials = vec![];
        let mut indices = vec![];
        for (i, source_material) in sm_source.materials.iter().enumerate() {
            let mat_file = match pack.lookup(&source_material.path)? {
                ggpk::ResolvedEntry::File(file) => file,
                _ => bail!("Material file {} is a directory", &source_material.path),
            };
            let mat = Rc::new(mat::parse_mat(mat_file.contents())?);
            // eprintln!("{:?}", &mat);
            let smd_material = draw::SmdMaterial::from_mat(&pack, &mat)
                .ok_or_else(|| format_err!("Could not load material"))?;
            // eprintln!("{:?}", &smd_material);
            sources.push(mat);
            materials.push(smd_material);
            indices.extend(std::iter::repeat(i).take(source_material.count));
        }
        (sources, materials, indices)
    };
    let smd_geom = draw::IndexedGeom::from_smd(&smd_source);
    let range_count = smd_source.shape_triangle_offsets.len();
    let smd_info = draw::SmdInfo {
        shape_ranges: {
            let offsets = &smd_source.shape_triangle_offsets;
            let index_starts: Vec<i32> = offsets
                .iter()
                .map(|off| *off as i32 * 3)
                .chain(std::iter::once(smd_geom.element_count as i32))
                .collect();
            index_starts
                .windows(2)
                .map(|offs| {
                    let from = offs[0];
                    let to = offs[1];
                    let len = to - from;
                    (from, len)
                })
                .collect()
        },
        shape_enables: std::iter::repeat(true).take(range_count).collect(),
        shape_materials: material_indices,
    };
    Ok(mode::LoadedSm {
        path,
        sm_source,
        smd_source,
        material_sources,
        smd_geom,
        smd_info,
        smd_materials,
    })
}

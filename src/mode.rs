use poe::formats::{arm, fmt, mat, sm, smd};
use super::draw;

use std::rc::Rc;

#[derive(Debug)]
pub struct SearchFilter {
    pub case_sensitive: bool,
    pub needle: imgui::ImString,
}

impl SearchFilter {
    pub fn predicate(&self) -> impl FnMut(&(usize, &imgui::ImStr)) -> bool {
        let search_keyword: String = if self.case_sensitive {
            self.needle.to_str().to_owned()
        } else {
            self.needle.to_str().to_lowercase()
        };
        let case_sensitive = self.case_sensitive;
        move |(_id, s)| -> bool {
            if case_sensitive {
                s.to_str().contains(&search_keyword)
            } else {
                s.to_str().to_lowercase().contains(&search_keyword)
            }
        }
    }
}

use poe::formats::dat::spec;

#[derive(Debug)]
pub struct DoodadVis {
    pub position: glm::Vec3,
    pub orientation: glm::Quat,
}

#[derive(Debug)]
pub enum Graph {
    Dgr(poe::formats::dgr::Dgr),
    Tgr(poe::formats::tgr::Tgr),
}

#[derive(Debug)]
pub struct HideoutData {
    pub hideout_dat: spec::Hideouts,
    pub small_world_area_dat: spec::WorldAreas,
    pub topology_dat: spec::Topologies,
    pub hideout: poe::formats::hideout::Hideout,
    pub graph: Graph,
    pub tsi: poe::formats::tsi::Tsi,
    pub rs: poe::formats::rs::Rs,
}

#[derive(Debug)]
pub struct HideoutMode {
    pub desired_index: usize,
    pub current_index: Option<usize>,
    pub current_data: Option<failure::Fallible<HideoutData>>,
}

#[derive(Debug)]
pub struct ItemMode {
    pub desired_index: usize,
    pub current_index: Option<usize>,
}

#[derive(Debug)]
pub struct SmMode {
    pub desired_index: usize,
    pub current_sm: Option<LoadedSm>,
}

#[derive(Debug)]
pub struct LoadedSm {
    pub path: String,
    pub sm_source: sm::Sm,
    pub smd_source: smd::Smd,
    pub material_sources: Vec<Rc<mat::Mat>>,
    pub smd_geom: draw::IndexedGeom,
    pub smd_info: draw::SmdInfo,
    pub smd_materials: Vec<draw::SmdMaterial>,
}

#[derive(Debug)]
pub struct ArmMode {
    pub desired_arm_index: usize,
    pub current_arm_index: Option<usize>,
    pub current_arm: Option<failure::Fallible<arm::Arm>>,
    pub current_mid: glm::Vec2,
    pub search_filter: SearchFilter,
    pub geom: Option<draw::ArmGeom>,
}

#[derive(Debug)]
pub struct FmtMode {
    pub desired_index: usize,
    pub current_index: Option<usize>,
    pub fmt: Option<fmt::Fmt>,
    pub draw_geom: Option<draw::IndexedGeom>,
    pub draw_info: Option<draw::SmdInfo>,
    pub materials: Vec<mat::Mat>,
}

#[derive(Debug)]
pub struct SmdMode {
    pub desired_index: usize,
    pub current_index: Option<usize>,
    pub current_smd: Option<failure::Fallible<smd::Smd>>,
    pub geom: Option<draw::IndexedGeom>,
    pub info: Option<draw::SmdInfo>,
}

#[derive(Debug)]
pub enum ProgramMode {
    Hideout(Box<HideoutMode>),
    Item(ItemMode),
    Sm(Box<SmMode>),
    Arm(Box<ArmMode>),
    Fmt(Box<FmtMode>),
    Smd(Box<SmdMode>),
}

impl ProgramMode {
    pub fn hideout() -> Option<Self> {
        Some(ProgramMode::Hideout(Box::new(HideoutMode {
            desired_index: 0,
            current_index: None,
            current_data: None,
        })))
    }

    pub fn item() -> Option<Self> {
        Some(ProgramMode::Item(ItemMode {
            desired_index: 0,
            current_index: None,
        }))
    }

    pub fn sm() -> Option<Self> {
        Some(ProgramMode::Sm(Box::new(SmMode {
            desired_index: 0,
            current_sm: None,
        })))
    }

    pub fn arm() -> Option<Self> {
        let search_needle = im_str!("/Unique/hideout.");
        let case_sensitive_search = false;
        Some(ProgramMode::Arm(Box::new(ArmMode {
            desired_arm_index: 0,
            current_arm_index: None,
            current_arm: None,
            current_mid: glm::vec2(0.0, 0.0),
            search_filter: SearchFilter { needle: search_needle.to_owned(), case_sensitive: case_sensitive_search },
            geom: None,
        })))
    }

    pub fn fmt() -> Option<Self> {
        Some(ProgramMode::Fmt(Box::new(FmtMode {
            desired_index: 16,
            current_index: None,
            fmt: None,
            draw_geom: None,
            draw_info: None,
            materials: vec![],
        })))
    }

    pub fn smd() -> Option<Self> {
        Some(ProgramMode::Smd(Box::new(SmdMode {
            // desired_index: 17_597,
            // desired_index: 17_436,
            desired_index: 0,
            current_index: None,
            current_smd: None,
            geom: None,
            info: None,
        })))
    }
}

impl ArmMode {
    pub fn change(&mut self, arm: Option<arm::Arm>) {
        if let Some(arm) = arm {
            let tile_size = 10.0;
            self.geom = Some(draw::ArmGeom::new(&arm, tile_size));
            self.current_mid = match &arm.k_1_by_1 {
                arm::KEntry::K { w, h, .. } => glm::vec2(*w as f32, *h as f32) * tile_size / 2.0,
                _ => glm::vec2(1.0, 1.0) * tile_size / 2.0,
            };
            self.current_arm = Some(Ok(arm));
        } else {
            self.geom = None;
            self.current_mid = glm::vec2(0.0, 0.0);
            self.current_arm = None;
        }
    }
}
extern crate nalgebra_glm as glm;

use super::geometry;
use super::render_gl;

use half::slice::HalfFloatSliceExt;

use std::ffi::CString;
use std::rc::Rc;

use poe::formats::{arm, fmt, ggpk, mat, smd};

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct ColorVertex {
    pub pos: glm::Vec3,
    pub clr: glm::Vec3,
}

impl ColorVertex {
    pub fn new(pos: glm::Vec3, clr: glm::Vec3) -> Self {
        ColorVertex { pos, clr }
    }
}

pub struct DiamondBuilder {
    mid: glm::Vec3,
    size: f32,
    color: glm::Vec3,
}

impl DiamondBuilder {
    pub fn new(mid: glm::Vec3) -> Self {
        DiamondBuilder {
            mid,
            size: 1.0,
            color: glm::vec3(1.0, 1.0, 1.0),
        }
    }

    pub fn size(self, size: f32) -> Self {
        Self { size, ..self }
    }

    pub fn color(self, color: glm::Vec3) -> Self {
        Self { color, ..self }
    }

    pub fn build(self) -> Vec<ColorVertex> {
        let DiamondBuilder {
            mid, size, color, ..
        } = &self;
        let r = size / 2.0;
        let mut v = vec![];
        let positions = [
            mid + glm::vec3(r, 0.0, 0.0),
            mid + glm::vec3(0.0, r, 0.0),
            mid - glm::vec3(r, 0.0, 0.0),
            mid - glm::vec3(0.0, r, 0.0),
        ];

        for i in 0..4 {
            v.push(ColorVertex::new(positions[i], *color));
            v.push(ColorVertex::new(positions[(i + 1) % 4], *color));
        }

        v
    }
}

pub struct GridBuilder {
    p0: glm::Vec2,
    p1: glm::Vec2,
    z: f32,
    major_color: glm::Vec3,
    minor_color: glm::Vec3,
    major_spacing: glm::Vec2,
    minor_ratio: usize,
}

impl GridBuilder {
    pub fn new(p0: glm::Vec2, p1: glm::Vec2, z: f32) -> Self {
        GridBuilder {
            p0,
            p1,
            z,
            major_color: glm::vec3(0.9, 0.9, 0.9),
            minor_color: glm::vec3(0.7, 0.7, 0.7),
            major_spacing: glm::vec2(100.0, 100.0),
            minor_ratio: 10,
        }
    }

    pub fn major_color(self, major_color: glm::Vec3) -> Self {
        Self {
            major_color,
            ..self
        }
    }

    pub fn minor_color(self, minor_color: glm::Vec3) -> Self {
        Self {
            minor_color,
            ..self
        }
    }

    pub fn uniform_major_spacing(self, spacing: f32) -> Self {
        Self {
            major_spacing: glm::vec2(spacing, spacing),
            ..self
        }
    }

    pub fn major_spacing(self, major_spacing: glm::Vec2) -> Self {
        Self {
            major_spacing,
            ..self
        }
    }

    pub fn minor_ratio(self, minor_ratio: usize) -> Self {
        Self {
            minor_ratio,
            ..self
        }
    }

    pub fn build(self) -> Vec<ColorVertex> {
        let GridBuilder { p0, p1, z, .. } = &self;
        let mut v = vec![];
        let minor_spacing = self.major_spacing / self.minor_ratio as f32;
        let xi_low = (p0.x / minor_spacing.x) as isize;
        let xi_high = (p1.x / minor_spacing.x) as isize;
        let yi_low = (p0.y / minor_spacing.y) as isize;
        let yi_high = (p1.y / minor_spacing.y) as isize;

        for xi in xi_low..=xi_high {
            let x = xi as f32 * minor_spacing.x;
            let color = if xi % self.minor_ratio as isize == 0 {
                self.major_color
            } else {
                self.minor_color
            };
            v.push(ColorVertex::new(glm::vec3(x, p0.y, *z), color));
            v.push(ColorVertex::new(glm::vec3(x, p1.y, *z), color));
        }

        for yi in yi_low..=yi_high {
            let y = yi as f32 * minor_spacing.y;
            let color = if yi % self.minor_ratio as isize == 0 {
                self.major_color
            } else {
                self.minor_color
            };
            v.push(ColorVertex::new(glm::vec3(p0.x, y, *z), color));
            v.push(ColorVertex::new(glm::vec3(p1.x, y, *z), color));
        }

        v
    }
}

pub struct ColorLineDrawer {
    prog: Rc<render_gl::Program>,
    vao: gl::types::GLuint,
    vbo: gl::types::GLuint,
    max_segment_count: usize,
    stride: usize,
}

impl Drop for ColorLineDrawer {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
            gl::DeleteBuffers(1, &self.vbo);
        }
    }
}

impl ColorLineDrawer {
    pub fn new(prog: Rc<render_gl::Program>, max_segment_count: usize) -> Self {
        let stride = std::mem::size_of::<ColorVertex>();
        let vao = unsafe {
            let mut id = 0;
            gl::CreateVertexArrays(1, &mut id);
            id
        };
        let vbo = unsafe {
            let mut id = 0;
            gl::CreateBuffers(1, &mut id);
            gl::NamedBufferStorage(
                id,
                (max_segment_count * 2 * stride) as gl::types::GLsizeiptr,
                std::ptr::null(),
                gl::DYNAMIC_STORAGE_BIT,
            );
            id
        };
        unsafe {
            gl::VertexArrayVertexBuffer(vao, 0, vbo, 0, stride as i32);
            for i in 0..2 {
                gl::EnableVertexArrayAttrib(vao, i);
                gl::VertexArrayAttribBinding(vao, i, 0);
            }
            let mut _offset_acc = 0;
            gl::VertexArrayAttribFormat(vao, 0, 3, gl::FLOAT, gl::FALSE, _offset_acc);
            _offset_acc += std::mem::size_of::<glm::Vec3>() as u32;
            gl::VertexArrayAttribFormat(vao, 1, 3, gl::FLOAT, gl::FALSE, _offset_acc);
            _offset_acc += std::mem::size_of::<glm::Vec3>() as u32;
        }
        ColorLineDrawer {
            prog,
            vao,
            vbo,
            max_segment_count,
            stride,
        }
    }

    pub fn draw(&self, projection: &glm::Mat4, view: &glm::Mat4, segments: &[ColorVertex]) {
        if segments.is_empty() {
            return;
        }
        let model: glm::Mat4 = glm::identity();
        let mvp: glm::Mat4 = projection * view * model;
        self.prog.use_program();
        unsafe {
            let mvp_name = CString::new("MVP").unwrap();
            let mvp_loc =
                gl::GetUniformLocation(self.prog.id, mvp_name.as_ptr() as *const gl::types::GLchar);
            gl::UniformMatrix4fv(
                mvp_loc,
                1,
                gl::FALSE,
                mvp.as_ptr() as *const gl::types::GLfloat,
            );
            gl::BindVertexArray(self.vao);
            for chunk in segments.chunks(self.max_segment_count * 2) {
                gl::NamedBufferSubData(
                    self.vbo,
                    0,
                    (chunk.len() * self.stride) as _,
                    chunk.as_ptr() as *const _,
                );
                gl::DrawArrays(gl::LINES, 0, chunk.len() as _);
            }
            gl::BindVertexArray(0);
        }
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct ArmVertex {
    pub pos: glm::Vec3,
    pub clr: glm::Vec3,
}

impl ArmVertex {
    pub fn new(pos: glm::Vec3, clr: glm::Vec3) -> Self {
        ArmVertex { pos, clr }
    }
}

#[derive(Debug)]
pub struct ArmGeom {
    vao: gl::types::GLuint,
    vbo: gl::types::GLuint,
    vertex_count: gl::types::GLsizei,
}

impl Drop for ArmGeom {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
            gl::DeleteBuffers(1, &self.vbo);
        }
    }
}

impl ArmGeom {
    pub fn new(source: &arm::Arm, tile_size: f32) -> Self {
        let vbo = unsafe {
            let mut id = 0;
            gl::CreateBuffers(1, &mut id);
            gl::NamedBufferData(id, 0, std::ptr::null(), gl::STATIC_DRAW);
            id
        };
        let vao = unsafe {
            let mut id = 0;
            gl::CreateVertexArrays(1, &mut id);
            let stride = (6 * std::mem::size_of::<f32>()) as gl::types::GLint;
            gl::VertexArrayVertexBuffer(id, 0, vbo, 0, stride);
            for i in 0..2 {
                gl::EnableVertexArrayAttrib(id, i);
            }
            gl::VertexArrayAttribFormat(id, 0, 3, gl::FLOAT, gl::FALSE, 0 as gl::types::GLuint);
            gl::VertexArrayAttribFormat(
                id,
                1,
                3,
                gl::FLOAT,
                gl::FALSE,
                (3 * std::mem::size_of::<f32>()) as gl::types::GLuint,
            );
            for i in 0..2 {
                gl::VertexArrayAttribBinding(id, i, 0);
            }
            id
        };

        use arm::KEntry::*;
        let k_xy = &source.k_x_by_y;
        let mut v = Vec::new();
        for (y, k_row) in k_xy.iter().enumerate() {
            for (x, k_col) in k_row.iter().enumerate() {
                let origin: glm::Vec3 = [
                    glm::lerp_scalar(0.0, tile_size, x as f32),
                    glm::lerp_scalar(0.0, tile_size, y as f32),
                    0.0,
                ]
                .into();
                let shift: glm::Vec3 = [tile_size * 0.5, tile_size * 0.5, 0.0].into();
                match k_col {
                    F(_n) => {
                        let tint: glm::Vec3 = [0.2, 0.4, 0.6].into();
                        let mut pyramid =
                            geometry::make_pyramid([tile_size, tile_size, 0.3].into());
                        pyramid.iter_mut().for_each(|v| {
                            v.pos += origin + shift;
                            v.clr.component_mul_assign(&tint);
                        });
                        v.extend(pyramid);
                    }
                    K { w, h, last, .. } => {
                        let w = *w as f32;
                        let h = *h as f32;
                        let half_tiles: glm::Vec3 = match last {
                            None | Some(0) => [w, h, 1.0],
                            Some(1) => [2.0 - w, h, 1.0],
                            Some(2) => [2.0 - w, 2.0 - h, 1.0],
                            Some(3) => [w, 2.0 - h, 1.0],
                            _ => panic!("unknown K-anchor point {:?}", last),
                        }
                        .into();
                        let big_shift = shift.component_mul(&half_tiles);
                        let mut pyramid =
                            geometry::make_pyramid([tile_size * w, tile_size * h, 0.3].into());
                        pyramid.iter_mut().for_each(|v| v.pos += origin + big_shift);
                        v.extend(pyramid);
                    }
                    N => {}
                    // N => {
                    //     let tint: glm::Vec3 = [0.5, 0.1, 0.1].into();
                    //     let mut pyramid = geometry::make_pyramid([tile_size, tile_size, 0.3].into());
                    //     pyramid.iter_mut().for_each(|v| {
                    //         v.pos += origin;
                    //         v.clr.component_mul_assign(&tint);
                    //     });
                    //     v.extend(pyramid);
                    // }
                    O => {
                        let tint: glm::Vec3 = [0.1, 0.5, 0.1].into();
                        let mut pyramid =
                            geometry::make_pyramid([tile_size, tile_size, 0.3].into());
                        pyramid.iter_mut().for_each(|v| {
                            v.pos += origin + shift;
                            v.clr.component_mul_assign(&tint);
                        });
                        v.extend(pyramid);
                    }
                    S => {
                        let tint: glm::Vec3 = [0.1, 0.1, 0.5].into();
                        let mut pyramid =
                            geometry::make_pyramid([tile_size, tile_size, 0.3].into());
                        pyramid.iter_mut().for_each(|v| {
                            v.pos += origin + shift;
                            v.clr.component_mul_assign(&tint);
                        });
                        v.extend(pyramid);
                    }
                }
            }
        }

        unsafe {
            let byte_size = (v.len() * 6 * std::mem::size_of::<f32>()) as gl::types::GLsizeiptr;
            gl::NamedBufferStorage(vbo, byte_size, v.as_ptr() as _, 0);
        }
        ArmGeom {
            vao,
            vbo,
            vertex_count: v.len() as i32,
        }
    }
}

pub struct ArmDrawer {
    prog: Rc<render_gl::Program>,
}

impl ArmDrawer {
    pub fn new(prog: Rc<render_gl::Program>) -> Self {
        ArmDrawer { prog }
    }

    pub fn draw(&self, projection: &glm::Mat4, view: &glm::Mat4, geom: &ArmGeom) {
        if geom.vertex_count > 0 {
            self.prog.use_program();
            let model: glm::Mat4 = glm::identity();
            let mvp: glm::Mat4 = projection * view * model;
            unsafe {
                let mvp_name = CString::new("MVP").unwrap();
                let mvp_loc = gl::GetUniformLocation(
                    self.prog.id,
                    mvp_name.as_ptr() as *const gl::types::GLchar,
                );
                gl::UniformMatrix4fv(
                    mvp_loc,
                    1,
                    gl::FALSE,
                    mvp.as_ptr() as *const gl::types::GLfloat,
                );
                gl::BindVertexArray(geom.vao);
                gl::DrawArrays(gl::TRIANGLES, 0, geom.vertex_count);
                gl::BindVertexArray(0);
            }
        }
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct FmtVertex {
    pub position: glm::Vec3,
    pub unknown1: glm::U8Vec4,
    pub unknown2: glm::U8Vec4,
    pub texcoord: glm::U16Vec2, // actually halfs, but no such type
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct SmdVertex {
    pub position: glm::Vec3,
    pub unknown1: glm::U8Vec4,
    pub unknown2: glm::U8Vec4,
    pub texcoord: glm::U16Vec2, // actually halfs, but no such type
    pub bone_indices: glm::U8Vec4,
    pub bone_weights: glm::U8Vec4,
}

#[derive(Debug)]
pub struct IndexedGeom {
    vao: gl::types::GLuint,
    vbo: gl::types::GLuint,
    ebo: gl::types::GLuint,
    pub vertex_count: gl::types::GLsizei,
    pub element_count: gl::types::GLsizei,
}

impl Default for IndexedGeom {
    fn default() -> Self {
        IndexedGeom {
            vao: 0,
            vbo: 0,
            ebo: 0,
            vertex_count: 0,
            element_count: 0,
        }
    }
}

impl IndexedGeom {
    pub fn from_fmt(source: &fmt::Fmt) -> IndexedGeom {
        let mut geom = IndexedGeom {
            vao: 0,
            vbo: 0,
            ebo: 0,
            vertex_count: 0,
            element_count: 0,
        };
        unsafe {
            gl::CreateVertexArrays(1, &mut geom.vao);
            gl::CreateBuffers(1, &mut geom.vbo);
            gl::CreateBuffers(1, &mut geom.ebo);
        }

        let indices: std::borrow::Cow<[u32]> = match &source.indices {
            fmt::Indices::Short(u16s) => {
                u16s.iter().map(|e| *e as u32).collect::<Vec<u32>>().into()
            }
            fmt::Indices::Long(u32s) => u32s.into(),
        };

        let v = source
            .vertices
            .iter()
            .map(|v| FmtVertex {
                position: v.position.into(),
                unknown1: glm::make_vec4(&v.unknown[0..4]),
                unknown2: glm::make_vec4(&v.unknown[4..8]),
                texcoord: glm::make_vec2(v.uv.reinterpret_cast()),
            })
            .collect::<Vec<FmtVertex>>();
        eprintln!("{} vertices, {} triangles", v.len(), indices.len() / 3);

        geom.vertex_count = v.len() as gl::types::GLsizei;
        geom.element_count = indices.len() as gl::types::GLsizei;
        unsafe {
            let stride = std::mem::size_of::<FmtVertex>();
            gl::NamedBufferStorage(
                geom.ebo,
                (indices.len() * std::mem::size_of::<u32>()) as gl::types::GLsizeiptr,
                indices.as_ptr() as *const gl::types::GLvoid,
                0,
            );
            gl::NamedBufferStorage(
                geom.vbo,
                (v.len() * stride) as gl::types::GLsizeiptr,
                v.as_ptr() as *const gl::types::GLvoid,
                0,
            );
            gl::VertexArrayElementBuffer(geom.vao, geom.ebo);
            gl::VertexArrayVertexBuffer(geom.vao, 0, geom.vbo, 0, stride as i32);
            for i in 0..4 {
                gl::EnableVertexArrayAttrib(geom.vao, i);
            }
            gl::VertexArrayAttribFormat(geom.vao, 0, 3, gl::FLOAT, gl::FALSE, 0);
            gl::VertexArrayAttribIFormat(geom.vao, 1, 4, gl::UNSIGNED_BYTE, 12);
            gl::VertexArrayAttribIFormat(geom.vao, 2, 4, gl::UNSIGNED_BYTE, 16);
            gl::VertexArrayAttribFormat(geom.vao, 3, 2, gl::HALF_FLOAT, gl::FALSE, 20);
            gl::VertexArrayAttribBinding(geom.vao, 0, 0);
            gl::VertexArrayAttribBinding(geom.vao, 1, 0);
            gl::VertexArrayAttribBinding(geom.vao, 2, 0);
            gl::VertexArrayAttribBinding(geom.vao, 3, 0);
        }

        geom
    }

    pub fn new(source: &smd::Smd) -> IndexedGeom {
        Self::from_smd(source)
    }

    pub fn from_smd(source: &smd::Smd) -> IndexedGeom {
        let mut geom = IndexedGeom {
            vao: 0,
            vbo: 0,
            ebo: 0,
            vertex_count: 0,
            element_count: 0,
        };
        let indices: std::borrow::Cow<[u32]> = match &source.triangle_indices {
            smd::Indices::Short(u16s) => {
                u16s.iter().map(|e| *e as u32).collect::<Vec<u32>>().into()
            }
            smd::Indices::Long(u32s) => u32s.into(),
        };

        let v = source
            .vertex_data
            .iter()
            .map(|v| SmdVertex {
                position: v.position.into(),
                unknown1: glm::make_vec4(&v.unknown[0..4]),
                unknown2: glm::make_vec4(&v.unknown[4..8]),
                texcoord: v.uv.into(),
                bone_indices: v.bone_indices.into(),
                bone_weights: v.bone_weights.into(),
            })
            .collect::<Vec<SmdVertex>>();
        eprintln!("{} vertices, {} triangles", v.len(), indices.len() / 3);

        geom.vertex_count = v.len() as gl::types::GLsizei;
        geom.element_count = indices.len() as gl::types::GLsizei;
        if geom.vertex_count == 0 || geom.element_count == 0 {
            return geom;
        }

        unsafe {
            gl::CreateVertexArrays(1, &mut geom.vao);
            gl::CreateBuffers(1, &mut geom.vbo);
            gl::CreateBuffers(1, &mut geom.ebo);

            let stride = std::mem::size_of::<SmdVertex>();
            gl::NamedBufferStorage(
                geom.ebo,
                (indices.len() * std::mem::size_of::<u32>()) as gl::types::GLsizeiptr,
                indices.as_ptr() as *const gl::types::GLvoid,
                0,
            );
            gl::NamedBufferStorage(
                geom.vbo,
                (v.len() * stride) as gl::types::GLsizeiptr,
                v.as_ptr() as *const gl::types::GLvoid,
                0,
            );
            gl::VertexArrayElementBuffer(geom.vao, geom.ebo);
            gl::VertexArrayVertexBuffer(geom.vao, 0, geom.vbo, 0, stride as i32);
            for i in 0..6 {
                gl::EnableVertexArrayAttrib(geom.vao, i);
            }
            gl::VertexArrayAttribFormat(geom.vao, 0, 3, gl::FLOAT, gl::FALSE, 0);
            gl::VertexArrayAttribIFormat(geom.vao, 1, 4, gl::UNSIGNED_BYTE, 12);
            gl::VertexArrayAttribIFormat(geom.vao, 2, 4, gl::UNSIGNED_BYTE, 16);
            gl::VertexArrayAttribFormat(geom.vao, 3, 2, gl::HALF_FLOAT, gl::FALSE, 20);
            gl::VertexArrayAttribIFormat(geom.vao, 4, 4, gl::UNSIGNED_BYTE, 24);
            gl::VertexArrayAttribFormat(geom.vao, 5, 4, gl::UNSIGNED_BYTE, gl::TRUE, 28);
            for i in 0..6 {
                gl::VertexArrayAttribBinding(geom.vao, i, 0);
            }
        }

        geom
    }
}

impl Drop for IndexedGeom {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
            gl::DeleteBuffers(1, &self.vbo);
            gl::DeleteBuffers(1, &self.ebo);
        }
    }
}

#[derive(Debug)]
pub struct SmdInfo {
    pub shape_enables: Vec<bool>,
    pub shape_ranges: Vec<(i32, i32)>,
    pub shape_materials: Vec<usize>,
}

#[derive(Debug)]
pub enum SmdMaterialChannel {
    RGBA,
    RGB,
    R,
    A,
}

type SmdMaterialSource = (Rc<render_gl::Texture>, SmdMaterialChannel);

#[derive(Debug)]
pub enum SmdBlendMode {
    Opaque,
    AlphaBlend,
}

#[derive(Debug)]
pub struct SmdMaterial {
    pub blend_mode: SmdBlendMode,
    pub color_tex: Option<SmdMaterialSource>,
    pub normal_tex: Option<SmdMaterialSource>,
    pub specular_tex: Option<SmdMaterialSource>,
}

fn resolve_dds_contents(pack: &ggpk::Ggpk, path: &str) -> Option<Vec<u8>> {
    // resolve symlinks
    let mut current_path = path.to_owned();
    let mut current_contents = None;
    while let Ok(file) = pack.lookup_file(&current_path) {
        let contents = file.contents();
        if contents.len() > 4 && contents.len() < 1024 && contents[0] == b'*' {
            current_path = std::str::from_utf8(&contents[1..]).ok()?.to_owned();
        } else {
            current_contents = poe::util::decompress_without_magic_word(contents).ok();
            break;
        }
    }
    current_contents
}

impl SmdMaterial {
    pub fn from_mat(pack: &ggpk::Ggpk, mat: &mat::Mat) -> Option<Self> {
        match mat {
            mat::Mat::V3(mat) => {
                let blend_mode = match mat.blend_mode.as_ref() {
                    "AlphaBlend" => SmdBlendMode::AlphaBlend,
                    _ => SmdBlendMode::Opaque,
                };
                let color_tex = mat.colour_texture.as_ref().and_then(|path| {
                    resolve_dds_contents(&pack, &path)
                        .and_then(|contents| render_gl::Texture::from_dds(&contents).map(Rc::new))
                });
                let normal_tex = mat.normal_texture.as_ref().and_then(|path| {
                    resolve_dds_contents(&pack, &path)
                        .and_then(|contents| render_gl::Texture::from_dds(&contents).map(Rc::new))
                });
                let specular_tex = mat.specular_texture.as_ref().and_then(|path| {
                    resolve_dds_contents(&pack, &path)
                        .and_then(|contents| render_gl::Texture::from_dds(&contents).map(Rc::new))
                });
                let normal_specular_tex = mat.normal_specular_texture.as_ref().and_then(|path| {
                    resolve_dds_contents(&pack, &path)
                        .and_then(|contents| render_gl::Texture::from_dds(&contents).map(Rc::new))
                });

                let (normal_tex, specular_tex) =
                    match (normal_tex, specular_tex, normal_specular_tex) {
                        (Some(n), None, None) => (Some((n, SmdMaterialChannel::RGB)), None),
                        (Some(n), Some(s), None) => (
                            Some((n, SmdMaterialChannel::RGB)),
                            Some((s, SmdMaterialChannel::R)),
                        ),
                        (None, Some(s), None) => (None, Some((s, SmdMaterialChannel::R))),
                        (None, None, Some(ns)) => (
                            Some((ns.clone(), SmdMaterialChannel::RGB)),
                            Some((ns.clone(), SmdMaterialChannel::A)),
                        ),
                        _ => (None, None),
                    };

                Some(SmdMaterial {
                    blend_mode,
                    color_tex: color_tex.map(|tex| (tex, SmdMaterialChannel::RGBA)),
                    normal_tex,
                    specular_tex,
                })
            }
            mat::Mat::V4(_mat) => None,
        }
    }
}

pub struct SmdDrawer {
    prog: Rc<render_gl::Program>,
}

pub struct ViewData<'o> {
    pub projection: &'o glm::Mat4,
    pub view: &'o glm::Mat4,
}

pub struct SpatialData<'o> {
    pub position: &'o glm::Vec3,
    pub orientation: &'o glm::Quat,
}

impl SmdDrawer {
    pub fn new(prog: Rc<render_gl::Program>) -> Self {
        SmdDrawer { prog }
    }

    pub fn draw(
        &self,
        view: ViewData,
        info: &SmdInfo,
        geom: &IndexedGeom,
        materials: &[SmdMaterial],
        spatial: SpatialData,
    ) -> failure::Fallible<()> {
        self.prog.use_program();
        let model: glm::Mat4 =
            glm::translate(&glm::quat_to_mat4(&spatial.orientation), spatial.position);
        let mvp: glm::Mat4 = view.projection * view.view * model;
        unsafe {
            let mvp_loc = gl::GetUniformLocation(
                self.prog.id,
                CString::new("MVP")?.as_ptr() as *const gl::types::GLchar,
            );
            let color_tex_loc = gl::GetUniformLocation(
                self.prog.id,
                CString::new("ColorTex")?.as_ptr() as *const gl::types::GLchar,
            );
            let normal_tex_loc = gl::GetUniformLocation(
                self.prog.id,
                CString::new("NormalTex")?.as_ptr() as *const gl::types::GLchar,
            );
            let specular_tex_loc = gl::GetUniformLocation(
                self.prog.id,
                CString::new("SpecularTex")?.as_ptr() as *const gl::types::GLchar,
            );

            gl::UniformMatrix4fv(
                mvp_loc,
                1,
                gl::FALSE,
                mvp.as_ptr() as *const gl::types::GLfloat,
            );

            gl::Uniform1i(color_tex_loc, 0);
            gl::Uniform1i(normal_tex_loc, 1);
            gl::Uniform1i(specular_tex_loc, 2);

            gl::BindVertexArray(geom.vao);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, geom.ebo);
            for (off, len, mat) in info.shape_ranges.iter().enumerate().filter_map(|(i, t)| {
                if info.shape_enables[i] {
                    let mat_idx = info.shape_materials[i];
                    Some((t.0, t.1, &materials[mat_idx]))
                } else {
                    None
                }
            }) {
                if len == 0 {
                    continue;
                }
                match mat.blend_mode {
                    SmdBlendMode::AlphaBlend => {
                        gl::Enable(gl::BLEND);
                        gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
                        gl::DepthMask(gl::FALSE);
                    }
                    SmdBlendMode::Opaque => {
                        gl::Disable(gl::BLEND);
                        gl::BlendFunc(gl::ONE, gl::ZERO);
                        gl::DepthMask(gl::TRUE);
                    }
                }
                gl::BindTextureUnit(0, mat.color_tex.as_ref().map(|tex| tex.0.id).unwrap_or(0));
                gl::BindTextureUnit(1, mat.normal_tex.as_ref().map(|tex| tex.0.id).unwrap_or(0));
                gl::BindTextureUnit(
                    2,
                    mat.specular_tex.as_ref().map(|tex| tex.0.id).unwrap_or(0),
                );
                gl::DrawElements(
                    gl::TRIANGLES,
                    len * 3,
                    gl::UNSIGNED_INT,
                    (off as usize * 3 * std::mem::size_of::<u32>()) as *const gl::types::GLvoid,
                );
            }
            gl::BindVertexArray(0);
        }
        Ok(())
    }
}

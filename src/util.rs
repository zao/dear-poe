use poe::formats::arm;

pub fn render_k_entry_text(k_col: &arm::KEntry) -> String {
    use arm::KEntry::*;
    match k_col {
        K { w, h, tail, last } => format!(
            r#"K {} {}
{:2} {:2} {:2} {:2}
{:2} {:2} {:2} {:2}
{:2} {:2} {:2} {:2}
{:2} {:2} {:2} {:2}
{:2} {:2} {:2} {:2}
{:2} {:?}
"#,
            w,
            h,
            tail[0],
            tail[1],
            tail[2],
            tail[3],
            tail[4],
            tail[5],
            tail[6],
            tail[7],
            tail[8],
            tail[9],
            tail[10],
            tail[11],
            tail[12],
            tail[13],
            tail[14],
            tail[15],
            tail[16],
            tail[17],
            tail[18],
            tail[19],
            tail[20],
            last,
        ),
        F(i) => format!("F {}", i),
        N => "N".to_owned(),
        O => "O".to_owned(),
        S => "S".to_owned(),
    }
}
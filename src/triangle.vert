#version 330 core

layout (location = 0) in vec3 Position;
layout (location = 1) in vec4 Unknown1;
layout (location = 2) in vec4 Unknown2;
layout (location = 3) in vec2 Texcoord;

uniform mat4 MVP;

out VS_OUTPUT {
    vec3 Normal;
    vec2 Texcoord;
} OUT;

void main()
{
    gl_Position = MVP * vec4(Position, 1.0);
    OUT.Normal = vec3(0.0, 0.0, -1.0);
    OUT.Texcoord = Texcoord;
}